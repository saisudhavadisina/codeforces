def winner(s, n):
    tem = [i for i in s if i == "A"]
    l, m = len(tem), n - len(tem)
    return "Anton" if l > m else "Danik" if l < m else "Friendship"
    
if __name__ == "__main__":
    n, s = int(input()), input()
    print(winner(s, len(s)))
