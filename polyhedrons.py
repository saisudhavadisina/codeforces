def polyhedrons(inputs):
    my_dict = {"Tetrahedron" : 4, "Cube" : 6, "Octahedron" : 8, "Dodecahedron" : 12, "Icosahedron" : 20}
    return sum([my_dict[i] for i in inputs])

if __name__ == "__main__":
    n = int(input())
    print(polyhedrons([str(input()) for i in range(n)]))
